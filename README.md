#TDOT Camera Viewer

This is a simple web app that displays the TDOT traffic camera that you are approaching. Camera data comes from https://smartway.tn.gov/traffic/. This in a completely in-browser app, and your location is never sent to any server.

Give it a try: http://jk3.us/tdot-camera-viewer/
