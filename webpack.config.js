var ExtractTextPlugin = require('extract-text-webpack-plugin');
var styleExtractor = new ExtractTextPlugin("styles.css")

module.exports = {
    entry: './src/tn_cams.js',
    output: {
        path: './dist',
        filename: 'app.bundle.js',
    },
    module: {
        loaders: [{
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'babel-loader'
        },{
            test: /\.scss$/,
			loader: styleExtractor.extract("style-loader", "css-loader!sass")
		}]
    },
	plugins: [styleExtractor]
}
